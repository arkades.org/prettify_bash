#!/bin/sh
AUTOCONF=false

dir="$(pwd)"
libdir=""
while [ "$dir" != "/" ]; do
  [ -d "$dir/automake" ] && libdir="$dir/automake" && break || dir="$(dirname "$dir")"
done

echo libdir: $libdir
touch AUTHORS NEWS README ChangeLog
[ -f PKGBUILD/PKGBUILD.in ] \
&& touch PKGBUILD/PKGBUILD.in

if [ configure.ac -nt Makefile.in ]; then
  echo aclocal \
  && rm -rf ./aclocal.m4 ./configure ./build ./config.status ./autom4te.cache \
  && aclocal
elif [ -d "$libdir/am" ]; then
  for am in $(find "$libdir/am" -maxdepth 1); do
    if [ "$am" -nt configure.ac ]; then
      echo aclocal \
      && touch Makefile.am \
      && touch configure.ac \
      && rm -rf ./aclocal.m4 ./configure ./build ./config.status ./autom4te.cache \
      && aclocal
      break
    fi
  done
fi

if [ Makefile.am -nt Makefile.in ] \
  || [ ! -f configure ]; then
  echo automake --libdir "$libdir" --add-missing --copy
  automake --libdir "$libdir" --add-missing --copy
  AUTOCONF=true
else
  
  for mf in $(find $(pwd) -maxdepth 2 -name Makefile.inc); do
    if [ "$mf" -nt Makefile.in ]; then
      echo automake --libdir "$libdir" --add-missing --copy
      automake --libdir "$libdir" --add-missing --copy
      AUTOCONF=true
      break
    fi
  done
fi

if [ ! -d ./build ]; then 
  mkdir ./build
fi

$AUTOCONF && autoconf
# $AUTOCONF && autoreconf -i && autoconf

