#!/bin/bash

echo configure.gnu $@

declare DISABLE_OPTION_CHECKING=""

TEMP=`getopt -o "" -l disable-option-checking,prefix:,sysconfdir:,localstatedir:,cache-file:,srcdir: -- "$@"`
eval set -- "$TEMP"

while [ "$1" ]; do
  case "$1" in
    --disable-option-checking)
    DISABLE_OPTION_CHECKING=--disable-option-checking; shift ;;
    --prefix)
    declare prefix=$(realpath $2) ; shift 2 ;;
    --sysconfdir)
    declare sysconfdir=$(realpath $2) ; shift 2 ;;
    --localstatedir)
    declare localstatedir=$(realpath $2) ; shift 2 ;;
    --cache-file)
    declare cachefile=$(realpath $2) ; shift 2 ;;
    --srcdir)
    declare srcdir=$(realpath $2) ; shift 2 ;;
    *)
    shift ;;
  esac
done

declare srcdir=${srcdir:-..}
declare prefix=${prefix:-/usr}
declare sysconfdir=${sysconfdir:-/etc}
declare localstatedir=${localstatedir:-/var}
declare cachefile=${cachefile:-/dev/null}

if [ "$(realpath ..)" == "$(realpath $srcdir)" ]; then
  [ -x ./autogen.sh ] && ./autogen.sh
  [ ! -d ./build ] && mkdir ./build
  cd ./build
else
  builddir=$(pwd)
  cd $srcdir
  [ -x ./autogen.sh ] && ./autogen.sh
  cd $builddir
fi

$srcdir/configure $DISABLE_OPTION_CHECKING --prefix="${prefix}" --sysconfdir="${sysconfdir}" --localstatedir="${localstatedir}" --cache-file="$cachefile" --srcdir="$srcdir"
